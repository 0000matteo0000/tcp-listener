#!/bin/bash
regex="."
conn_dir="flows"
save_dir="saved"
likely_previous_n=5


clear; clear;

trap leave SIGINT
leave() {
    sudo kill $(jobs -p)
    exit
}


# check if connection file matches
check_match() {
    while IFS= read -r conn; do
        # check if some match the regex
        if grep -qEie "$regex" "$conn"; then
            echo "$conn matches"
        fi
    done
}


# gets previous packets, args: save folder, previous_n
get_likely_previous(){
    pd="$1"
    n="$2"
    mkdir --parents "$pd"
    pr="$(echo "$conn" | sed -E 's/^([0-9]{,3}\.[0-9]{,3}\.[0-9]{,3}\.[0-9]{,3})\.([0-9]{,5})-([0-9]{,3}\.[0-9]{,3}\.[0-9]{,3}\.[0-9]{,3})\.([0-9]{,5})c?([0-9]*)?$/\/?(\1|\3)\\..*?-(\1|\3)\\..*?c?[0-9]*$/p')"
    relevant_connections="$(find "$conn_dir" -type f -iregex "$r" -not -newermm "$conn" | xargs -L1 stat --format="%Y %n" | sort | awk '{print $2; fflush()} NR=='"$n"'{exit}')"
    cp --target-directory="$pd" "$relevant_connections" && echo "$conn saved likely previous"
}


# save matching connection
save() {
    while IFS= read -r conn; do
        # save connection files
        r="$(echo "$conn" | sed -E 's/^([0-9]{,3}\.[0-9]{,3}\.[0-9]{,3}\.[0-9]{,3})\.([0-9]{,5})-([0-9]{,3}\.[0-9]{,3}\.[0-9]{,3}\.[0-9]{,3})\.([0-9]{,5})c?([0-9]*)?$/\/?(\1|\3)\\.(\2|\4)-(\1|\3)\\.(\2|\4)c?[0-9]*$/p')"
        d="$save_dir/$(date +'%F_%T')/"
        mkdir --parents "$d"
        cp --target-directory="$d" "$(find "$conn_dir" -type f -iregex "$r" )" && echo "$conn saved"
        get_likely_previous "$d/previous" "$likely_previous_n"
    done
}


# remove connections older than $1 or 60 seconds, checks every $2 or 10 seconds
remove_older_files() {
    while sleep "${2:-10}"; do
        # delete older files
        while read -r conn; do
            if [ ! -z "$conn" ]; then
                rm "$conn" && echo "$conn deleted because it was old"
            fi
        done <<< "$(find "$conn_dir" -type f -not -iname "report*" -not -newermt "-${1:-60} seconds")"
    done
}


flows_log="flows_log.txt"
mkdir --parents "$conn_dir" "$save_dir"
remove_older_files "60" "10" &
# log every time a new flow is closed
rm "$flows_log" && touch "$flows_log"
inotifywait --format '%T %w%f %e' --timefmt '%F_%T' --monitor --event close_write --recursive "$conn_dir" --outfile "$flows_log" &
# log tcp flows
sudo tcpflow -a -o "$conn_dir" &
tail -f "$flows_log" | tee /dev/tty | # for each new connection, echo
awk '{print $2; fflush()}' | check_match | tee /dev/tty | # format, check flow match and echo
awk '{print $1; fflush()}' | save # format and save matching flow files, echoes


leave
